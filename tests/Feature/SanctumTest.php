<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SanctumTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_be_retrived()
    {
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->getJson('/api/user');

        // $response->dump();
        $response->assertOk();
    }
    public function test_user_not_found()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->get('/api/user');

        // $response->dump();
        $response->assertUnauthorized();
    }
}
