<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Goal;
use App\Models\Task;
use App\Models\User;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GoalApiTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetAllGoalsForCurrentUser()
    {
        $user = User::factory()->create();
        $goal = Goal::factory()->create(['user_id' => $user->id]);
        
        $user_2 = User::factory()->create();
        $goal_2 = Goal::factory()->create(['user_id' => $user_2->id]);
        Task::factory()->create(['goal_id'=>$goal->id, 'user_id'=> $user_2->id]);
        Task::factory()->create(['goal_id'=>$goal->id, 'user_id'=> $user->id]);
        Task::factory()->create(['goal_id'=>$goal->id, 'user_id'=> $user->id]);
        
        Sanctum::actingAs(
            $user,
            ['*']
        );
        
        $response = $this->getJson('/api/goal');

        $this->assertAuthenticated();

        // $response->dump();
        
        $this->assertAuthenticatedAs($user);
        $response->assertOk()
            ->assertJsonCount(1)
            ->assertJsonPath('0.user_id', $user->id)
            ;
    }
    public function testCreateAGoal()
    {
        $user_0 = User::factory()->create();
        $user = User::factory()->create();

        $goal = Goal::factory()->make();
        
        Sanctum::actingAs(
            $user,
            // ['*']
        );
        $response = $this->postJson('/api/goal', $goal->toArray());

        // $response->dump();
        
        $this->assertAuthenticatedAs($user);
        $response->assertStatus(201)
            ->assertJsonStructure([
                'name',
                'symbol',
                'description',
                'user_id',
            ])
            ;
        $this->assertDatabaseCount('goals', 1);
        $this->assertDatabaseHas('goals', [
            'name' => $goal->name,
            'description' => $goal->description,
            'symbol' => $goal->symbol,
            'user_id' => $user->id,
        ]);
    }
    public function testUpdateAGoal()
    {
        $user = User::factory()->create();

        $goal = Goal::factory()->create();
        $goal_2 = Goal::factory()->make();

        Sanctum::actingAs(
            $user,
            // ['*']
        );

        $response = $this->putJson('/api/goal/' . $goal->id, $goal_2->toArray());

        // $response->dump();
        
        $this->assertAuthenticatedAs($user);

        $response->assertStatus(200)
            ->assertJsonStructure([
                'name',
                'symbol',
                'description',
                'user_id',
            ])
            ;
        $this->assertDatabaseCount('goals', 1);
        $this->assertDatabaseHas('goals', [
            'name' => $goal_2->name,
            'description' => $goal_2->description,
            'symbol' => $goal_2->symbol,
            'user_id' => $user->id,
        ]);
    }
    public function testCannotUpdateAGoalOfAnotherUser()
    {
        $user = User::factory()->create();
        $author = User::factory()->create();

        $goal = Goal::factory()->create(['user_id' => $author->id]);

        Sanctum::actingAs(
            $user,
            // ['*']
        );

        $response = $this->putJson('/api/goal/' . $goal->id, $goal->toArray());

        // $response->dump();
        
        $this->assertAuthenticatedAs($user);

        $response->assertStatus(404);
        $this->assertDatabaseCount('goals', 1);
        $this->assertDatabaseHas('goals', [
            'name' => $goal->name,
            'description' => $goal->description,
            'symbol' => $goal->symbol,
            'user_id' => $author->id,
        ]);
    }
    public function testCanDeleteOwnGoal()
    {
        $user = User::factory()->create();
        $goal = Goal::factory()->create(['user_id' => $user->id]);

        Sanctum::actingAs(
            $user,
            // ['*']
        );

        $response = $this->deleteJson('/api/goal/' . $goal->id);

        // $response->dump();
        
        $this->assertAuthenticatedAs($user);

        $response->assertStatus(200);
        $this->assertDatabaseCount('goals', 0);
        $this->assertDatabaseMissing('goals', [
            'name' => $goal->name,
            'description' => $goal->description,
            'symbol' => $goal->symbol,
            'user_id' => $user->id,
        ]);
    }
    public function testCannotDeleteAGoalOfAnotherUser()
    {
        $user = User::factory()->create();
        $author = User::factory()->create();
        $goal = Goal::factory()->create(['user_id' => $author->id]);

        Sanctum::actingAs(
            $user,
            // ['*']
        );

        $response = $this->deleteJson('/api/goal/' . $goal->id);

        // $response->dump();
        
        $this->assertAuthenticatedAs($user);

        $response->assertStatus(404);
        $this->assertDatabaseCount('goals', 1);
        $this->assertDatabaseHas('goals', [
            'name' => $goal->name,
            'description' => $goal->description,
            'symbol' => $goal->symbol,
            'user_id' => $author->id,
        ]);
    }
}
