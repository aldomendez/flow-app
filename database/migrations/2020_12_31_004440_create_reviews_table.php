<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->foreignId('last_week');
            $table->foreignId('next_week');
            $table->foreignId('sunday_goal');

            $table->foreignId('user_id');
            $table->timestamps();

            $table->index('user_id');
            $table->foreign('last_week')->references('id')->on('goals');
            $table->foreign('next_week')->references('id')->on('goals');
            $table->foreign('sunday_goal')->references('id')->on('goals');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
