<?php

namespace Database\Seeders;

use App\Models\Goal;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $users = User::factory()->count(10)->create();

        $users->each(function ($user){
            // dd($user);
            $goals = collect([
                Goal::factory()->create(['name'=>'Relationship','symbol'=>'heart','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Kids and family','symbol'=>'family','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Health','symbol'=>'drop','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Financial freedom','symbol'=>'money','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Friends','symbol'=>'happy_face','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Fullfilling profession','symbol'=>'briefcase','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Personal development','symbol'=>'graph_line','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Joy and hobbies','symbol'=>'sun','user_id'=>$user->id]),
                Goal::factory()->create(['name'=>'Necessities','symbol'=>'checkmark','user_id'=>$user->id]),
            ])->concat(Goal::factory()->count(rand(3, 10))
            ->create(['user_id'=>$user->id]));

            $goals->each(function ($goal) use ($user){
                
                Task::factory()->count(rand(0, 15))
                    ->create([
                        'goal_id' => $goal->id,
                        'user_id' => $goal->user_id,
                    ]);
            });
        });
    }
}
