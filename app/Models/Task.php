<?php

namespace App\Models;

use App\Scopes\UserScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'description',
        'completed',
        'goal_id',
        'user_id',
    ];

    protected $casts = [
        'competed' => 'boolean',
        'user_id' => 'integer',
        'goal_id' => 'integer',
    ];

    public function goal(){
        return $this->belongsTo(Goal::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    protected static function booted()
    {
        static::addGlobalScope(new UserScope);
    }
}
