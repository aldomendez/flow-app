<?php

namespace App\Models;

use App\Models\Goal;
use App\Models\Task;
use App\Models\User;
use App\Scopes\UserScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Goal extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'symbol',
        'description',
        'user_id',
    ];

    protected $casts = [
        'user_id' => 'integer',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function tasks(){
        return $this->hasMany(Task::class);
    }
    
    protected static function booted()
    {
        static::addGlobalScope(new UserScope);
    }
}
