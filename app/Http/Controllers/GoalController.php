<?php

namespace App\Http\Controllers;

use App\Models\Goal;
use Illuminate\Http\Request;

class GoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Goal::with('tasks')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name' => 'required',
            'symbol' => 'required',
            'description' => 'required',
            'user_id' => 'required',
        ]);
        
        $user = Auth()->user();

        $base = collect(
            $request->only([
                'name',
                'symbol',
                'description',
            ])
        )->merge(['user_id'=>$user->id])->all();

        $goal = Goal::create(
            $base
        );

        return $goal;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Goal  $goal
     * @return \Illuminate\Http\Response
     */
    public function show(Goal $goal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Goal  $goal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Goal $goal)
    {
        $request->validate([
            'name' => 'required',
            'symbol' => 'required',
            'description' => 'required',
            'user_id' => 'required',
        ]);
        
        $user = Auth()->user();

        $goal->update(
            $request->only([
                'name',
                'symbol',
                'description',
            ])
        );

        return $goal;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Goal  $goal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Goal $goal)
    {
        return $goal->delete();
    }
}
